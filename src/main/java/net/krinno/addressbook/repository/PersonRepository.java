package net.krinno.addressbook.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import net.krinno.addressbook.model.Person;


/**
 * @author KRINNO
 *
 */

@Repository
public class PersonRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<Person> contactList() {
	
		Session session = this.sessionFactory.getCurrentSession();
		List<Person> contactList = session.createQuery("from Person").list();
		
		return contactList;
	}
	
	
	public Person getPersonByID(int personID) {
		
		Session session = this.sessionFactory.getCurrentSession();
		Person person = (Person) session.get(Person.class, personID);
		return person;
	}
	
	// !!!
	public Person getPersonByLastname(String lastname) {
		
		Session session = this.sessionFactory.getCurrentSession();
		Person person = (Person) session.get(Person.class, lastname);
		return person;
	}
	
	// !!!
	public Person getPersonByFirstname(String firstname) {
		
		Session session = sessionFactory.getCurrentSession();			
		String hql = "FROM Person pers WHERE pers.firstname = :firstname";
		
		Query query = session.createQuery(hql);		
		query.setParameter("firstname", firstname);
		
		Person person = (Person) query.uniqueResult();
		return person;
	}
	
	
	public void addPerson(Person person) {
		
		Session session = this.sessionFactory.getCurrentSession();
	    session.persist(person);				
	}
	
	public void editPerson(Person editPerson) {
		
		Session session = this.sessionFactory.getCurrentSession();
		Person person = (Person) session.get(Person.class, editPerson.getID());		
		
		person.setFirstname(editPerson.getFirstname());
		person.setLastname(editPerson.getLastname());
		person.setEmail(editPerson.getEmail());
		person.setPhone(editPerson.getPhone());
		
		session.flush();
		session.save(person);
	}
	
	
	public void deletePerson(Integer userID) {
			
		Session session = this.sessionFactory.getCurrentSession();
		Person person = (Person) session.load(Person.class, new Integer(userID));
		
        if(null != person){
            session.delete(person);
        }
	}

}
