package net.krinno.addressbook.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import net.krinno.addressbook.model.Person;
import net.krinno.addressbook.repository.PersonRepository;


/**
 * @author KRINNO
 *
 */

@Service
public class PersonService {
	
	@Autowired
	private PersonRepository personRepository;


	@Transactional
	public List<Person> contactList() {
		return this.personRepository.contactList();
	}
	
	@Transactional
	public Person getPersonByID(int personID) {
		return personRepository.getPersonByID(personID);
	}
	
	@Transactional
	public Person getPersonByLastname(String lastname) {
		return personRepository.getPersonByLastname(lastname);
	}
	
	@Transactional
	public Person getPersonByFirstname(String firstname) {
		return personRepository.getPersonByFirstname(firstname);
	}
	
	
	@Transactional
	public void addPerson(Person person) {
		this.personRepository.addPerson(person);
	}
	
	@Transactional
	public void editPerson(Person person) {
		this.personRepository.editPerson(person);
	}
	
	@Transactional
	public void deletePerson(int personID) {
		this.personRepository.deletePerson(personID);
	}
}
