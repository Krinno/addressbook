package net.krinno.addressbook.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * @author KRINNO
 *
 */

@Configuration
@ComponentScan({ "net.krinno.addressbook.*" })
@Import({ MvcConfig.class, HibernateConfig.class })
public class AppConfig {

}
