package net.krinno.addressbook.controller;


import java.util.List;

import javax.validation.Valid;

import net.krinno.addressbook.model.Person;
import net.krinno.addressbook.service.PersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Handles requests for the application home page.
 * @author KRINNO
 */

@Controller
public class HomeController {
	
	@Autowired
    private PersonService personService;
	
	// ROOT	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String root() {
		return "redirect:/index";
	}
	
	// SHOW LIST
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String contactList(Model model) {		
		
		List<Person> contactList = this.personService.contactList();
		model.addAttribute("contactList", contactList);			
		return "index";
	}

		
	// DELETE
	@RequestMapping(value = "/delete/{personID}", method = RequestMethod.GET)
	public String deletePerson(@PathVariable int personID) {

			personService.deletePerson(personID);	        
	        return "redirect:/index";
	}

	    
	// ADD
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addPersonGET(Model model) {
	    	
		model.addAttribute("person", new Person());
		return "add";
	}	
	    
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addPersonPOST(@Valid @ModelAttribute("person") Person person, 
	    					  	BindingResult bindingResult) {
	    	
		if (bindingResult.hasErrors()) {
			return "add";
		} else {
			personService.addPerson(person);	    		
	    	return "redirect:/index";
	    }
	}
	
	    
	// EDIT
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editTaskGET(@RequestParam("personID") int personID, Model model) {
	    	
		model.addAttribute("person", personService.getPersonByID(personID)); 
		return "edit";	
	}
	    	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String editTask(@RequestParam("personID") int personID,
							@Valid @ModelAttribute("person") Person person,
	    					   BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			person.setID(personID);
			return "edit";
		} 
		else {
			System.out.println("I'm here");
			person.setID(personID);
			personService.editPerson(person);
			System.out.println("I'm jere");
	    	return "redirect:/index";
	    }
	}
}
