<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>		
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html>
<head>
	<title>Add person</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">		
	<link href="resources/error.css" rel="stylesheet">	
</head>
	
<body>	
		<h2>Add new person:</h2>

		<form:form method="post" modelAttribute="person" action="add">
		<table>
		<tr>
			<th><form:label path="lastname">Lastname:</form:label></th>
			<th><form:input path="lastname" /></th>
			<th><form:errors path="lastname" cssClass="error"/></th>
		</tr>
		<tr>
			<th><form:label path="firstname">Firstname:</form:label></th>
			<th><form:input path="firstname" /></th>
			<th><form:errors path="firstname" cssClass="error"/></th>
		</tr>
		<tr>	
			<th><form:label path="email">E-Mail:</form:label></th>
			<th><form:input path="email" /></th>
			<th><form:errors path="email" cssClass="error"/></th>
		</tr>
		<tr>	
			<th><form:label path="phone">Phone:</form:label></th>
			<th><form:input path="phone" /></th>
			<th><form:errors path="phone" cssClass="error"/></th>
		</tr>
		</table>
		<button type="submit">Save</button>
		
</form:form>

</body>
</html>