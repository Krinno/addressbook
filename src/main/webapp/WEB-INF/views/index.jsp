<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>
<head>
	<title>AddressBook</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<h2>Address book:</h2>
<c:if test="${empty contactList}">
<h4>Adress book is empty :(</h4>
</c:if>

<a href="add">Add new person</a><hr>
<c:if test="${!empty contactList}">
		<table border="1">	
		<tr>
			<th>Lastname</th>
			<th>Firstname</th>
			<th>E-Mail</th>
			<th>Phone(s)</th>
			<th>Actions</th>
		</tr>
		 <c:forEach items="${contactList}" var="person"> 
			<tr>
				<td>${person.lastname}</td>
				<td>${person.firstname}</td>
				<td>${person.email}</td>
				<td>${person.phone}</td>
				<td><a href="edit?personID=${person.ID}">Edit</a>				
					<a href="delete/${person.ID}" 
					onclick="return confirm('Are you sure?')">Delete</a></td>		
			</tr>		
		 </c:forEach> 
		
	</table>
	</c:if>
</body>
</html>
